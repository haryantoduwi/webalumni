<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class Agenda extends CI_Controller
	{
		
		function __construct(){
			parent::__construct();
			$this->load->model('Crud');
			if(($this->session->userdata('level')!=1) AND ($this->session->userdata('login')==true)){
				redirect(site_url('login/logout'));
			}	
			$this->url="backend/agenda";		
		}
		function konversimatauang($var){
			$var=str_replace('Rp ', '', $var);
			$var=str_replace('.', '', $var);
			return $var;
		}
		function atributmenu(){
			$data=array(
				'menu'=>'agenda',
				'headline'=>'Daftar Agenda',
				'edit'=>'Edit Agenda',
				'add'=>"Add Agenda",
				'icon'=>'fa fa-calendar',
				'breadcrumb'=>'backend/agenda',
				'action'=>'backend/agenda',
				);
			//KONVERT ARRAY TO OBJECT
			return $data=(object)$data;	
		}		
		function index(){
			if($this->input->post('submit')){
				$data=array(
					'nama'=>$this->input->post('nama'),
					'tanggal'=>date('Y-m-d',strtotime($this->input->post('tanggal'))),
					'keterangan'=>$this->input->post('editor'),
					'save_date'=>date('Y-m-d H:i' ),
				);
				$simpan=$this->Crud->simpan('agenda',$data);
				if($simpan){
					$this->session->set_flashdata('success','berhasil');
				}else{
					$error=$this->db->error();
					$this->session->set_flashdata('error',$error['message']);
				}												
			}
			$data=array(
				'menu'=>$this->atributmenu(),
				'data'=>$this->Crud->get_data('agenda')->result(),
			);			
			$this->load->view('backend',$data);
		}
		function update(){
			$id=$this->input->post('id');
			$data=array(
				'nama'=>$this->input->post('nama'),
				'tanggal'=>date('Y-m-d',strtotime($this->input->post('tanggal'))),
				'keterangan'=>$this->input->post('editor'),
				'update_date'=>date('Y-m-d H:i' ),
			);			
			$update=$this->Crud->update('agenda',$id,$data);
			if($update){
				$this->session->set_flashdata('success','berhasil');
			}else{
				$error=$this->db->error();
				$this->session->set_flashdata('error',$error['message']);
			}
			redirect(site_url($this->url));			
		}	
		function hapus($id){
			$hapus=$this->Crud->hapus('agenda',$id);
			if($hapus==true){
				$this->session->set_flashdata('success','berhasil');
			}else{
				$this->session->set_flashdata('error',$hapus);
			}
			redirect(site_url($this->url));
		}
		function edit(){
			$id=$this->input->post('id');
			$data=array(
				'menu'=>$this->atributmenu(),
				'data'=>$this->Crud->edit('agenda',md5($id))->row(),
			);
			$this->load->view('backend/agenda/edit',$data);
			//print_r($data['data']);
		}
	}
?>