<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class Alumni extends CI_Controller
	{
		
		function __construct(){
			parent::__construct();
			$this->load->model('Crud');
			if($this->session->userdata('level')!=1){
				redirect(site_url('login/logout'));
			}			
		}
		function atributmenu(){
			$data=array(
				'menu'=>'alumni',
				'headline'=>'Tampil alumni',
				'icon'=>'fa fa-users',
				'breadcrumb'=>'backend/Alumni',
				);
			//KONVERT ARRAY TO OBJECT
			return $data=(object)$data;	
		}		
		function index(){
			$data=array(
				'menu'=>$this->atributmenu(),
				'user'=>$this->Crud->get_data('alumni')->result(),
				);		
			$this->load->view('backend',$data);		
		}		
		function simpan(){
			$id=$this->input->post('id');
			$data=array(
				'nama'=>$this->input->post('nama'),
				'username'=>$this->input->post('username'),
				'password'=>md5($this->input->post('password')),
				'level'=>$this->input->post('level'),
			);
			if(empty($id)){
				$data['save_date']=date('Y-m-d H:i' );
				$simpan=$this->Crud->simpan('alumni',$data);
				//echo "simpan";	
			}else{
				$data['update_date']=date('Y-m-d H:i');
				$simpan=$this->Crud->update('alumni',$id,$data);
				//echo "upate";
			}
			if($simpan){
				$this->session->set_flashdata('success','berhasil');
			}else{
				$error=$this->db->error();
				$this->session->set_flashdata('error',$error['message']);
			}
			redirect(site_url("Alumni"));
		}
		function hapus($id){
			$hapus=$this->Crud->hapus('alumni',$id);
			if($hapus==true){
				$this->session->set_flashdata('success','berhasil');
			}else{
				$this->session->set_flashdata('error',$hapus);
			}
			redirect(site_url("Alumni"));
		}
		function edit(){
			$id=$this->input->post('id');
			$data=array(
				'menu'=>'User',
				'alumni'=>$this->Crud->edit('alumni',$id)->row(),
			);
			$this->load->view('alumni/edit',$data);
		}
	}
?>