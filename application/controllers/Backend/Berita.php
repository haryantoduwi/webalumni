<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class Berita extends CI_Controller
	{
		
		function __construct(){
			parent::__construct();
			$this->load->model('Crud');
			if(($this->session->userdata('level')!=1) AND ($this->session->userdata('login')==true)){
				redirect(site_url('login/logout'));
			}	
			$this->url="backend/Berita";
			$this->tabel="berita";		
		}
		function konversimatauang($var){
			$var=str_replace('Rp ', '', $var);
			$var=str_replace('.', '', $var);
			return $var;
		}
		function atributmenu(){
			$data=array(
				'menu'=>'berita',
				'headline'=>'Daftar Berita',
				'edit'=>'Edit Berita',
				'add'=>"Add Berita",
				'icon'=>'fa fa-newspaper-o',
				'breadcrumb'=>'backend/berita',
				'action'=>'backend/berita',
				);
			//KONVERT ARRAY TO OBJECT
			return $data=(object)$data;	
		}		
		function index(){
			if($this->input->post('submit')){
				$kategori=implode(',', $this->input->post('id_kategori'));
				$data=array(
					'judul'=>$this->input->post('judul'),
					'id_kategori'=>$kategori,
					'deskripsi'=>$this->input->post('editor'),
					'save_date'=>date('Y-m-d H:i' ),
				);
				$simpan=$this->Crud->simpan($this->tabel,$data);
				if($simpan){
					$this->session->set_flashdata('success','berhasil');
				}else{
					$error=$this->db->error();
					$this->session->set_flashdata('error',$error['message']);
				}												
			}
			$data=$this->Crud->get_data($this->tabel)->result();
			$a=0;	
			for($i=0;$i<count($data); $i++) { 
				$data[$i]=$data[$i];
				$id_kategori[$i]=explode(',', $data[$i]->id_kategori);
				foreach ($id_kategori[$i] as $key) {
					$select='kategori';
					$tabel='kategori_berita';
					$where=array('id'=>$key);
					$query=$this->Crud->get_where($select,$tabel,$where)->row();
					$kategori=$query->kategori;
					$dt[$i][]=$kategori;//karena data yang diambil dalam bentuk array
				}
				$kategoridt[$i]=implode(', ',$dt[$i]);

				$data[$i]->nmkategori=$kategoridt[$i];
			}
			//print_r($data);
			$data=array(
				'menu'=>$this->atributmenu(),
				'kategori'=>$this->Crud->get_data('kategori_berita')->result(),
				'data'=>$data,
			);	
			$this->load->view('backend',$data);
		}
		function update(){
			$id=$this->input->post('id');
			$id_kategori=implode(',', $this->input->post('id_kategori'));
			$data=array(
				'judul'=>$this->input->post('judul'),
				'id_kategori'=>$id_kategori,
				'deskripsi'=>$this->input->post('editor'),
				'update_date'=>date('Y-m-d H:i' ),
			);			
			$update=$this->Crud->update($this->tabel,$id,$data);
			if($update){
				$this->session->set_flashdata('success','berhasil');
			}else{
				$error=$this->db->error();
				$this->session->set_flashdata('error',$error['message']);
			}
			redirect(site_url($this->url));			
		}	
		function hapus($id){
			$hapus=$this->Crud->hapus($this->tabel,$id);
			if($hapus==true){
				$this->session->set_flashdata('success','berhasil');
			}else{
				$this->session->set_flashdata('error',$hapus);
			}
			redirect(site_url($this->url));
		}
		function edit(){
			$id=$this->input->post('id');
			// $kategori=$this->Crud->get_data('kategori_berita')->result();
			// $data=$this->Crud->edit($this->tabel,md5($id))->row();
			// for($i=0;$i<count($kategori);$i++){
			// 	$value[$i]=$kategori[$i];
			// 	$val=explode(',', $data->id_kategori);
			// 	if(isset($val[$i])){
			// 		echo $val[$i].'-'.$value[$i]->id.'/'.$value[$i]->kategori;
			// 	}else{
			// 		echo "no";
			// 	}
			// }
			$data=array(
				'menu'=>$this->atributmenu(),
				'data'=>$this->Crud->edit($this->tabel,md5($id))->row(),
				'kategori'=>$this->Crud->get_data('kategori_berita')->result(),
			);
			$this->load->view('backend/berita/edit',$data);
			//print_r($val);
		}
	}
?>