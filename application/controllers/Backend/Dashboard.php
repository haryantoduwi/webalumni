<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class Dashboard extends CI_Controller
	{
		
		function __construct(){
			parent::__construct();
			$this->load->model(array('Crud'));
			if($this->session->userdata('level')!=1){
				redirect(site_url('Login/logout'));
			}
		}
		function atributmenu(){
			$data=array(
				'menu'=>'dashboard',
				'headline'=>'administrator',
				'icon'=>'fa fa-dashboard',
				'breadcrumb'=>'backend/Dashboard',
				);
			//KONVERT ARRAY TO OBJECT
			return $data=(object)$data;	
		}		
		function index(){
			$data=array(
				'menu'=>$this->atributmenu(),
			);
			$this->load->view('backend',$data);
		}
		function dashboard(){
			redirect(site_url('Dashboard'));
		}		
		function user(){
			$data=array(
				'menu'=>"user",
				'headline'=>"Tampil User",
				'user'=>$this->Crud->get_data('user')->result(),
				);
			$this->load->view('backend',$data);
		}
		function pendaftar(){
			$data=array(
				'menu'=>"pendaftar",
				'headline'=>"Tampil pendaftar",
				'user'=>$this->Crud->get_data('pendaftar')->result(),
				);		
			$this->load->view('backend',$data);		
		}
	}
?>