<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class Kategoriberita extends CI_Controller
	{
		
		function __construct(){
			parent::__construct();
			$this->load->model('Crud');
			if(($this->session->userdata('level')!=1) AND ($this->session->userdata('login')==true)){
				redirect(site_url('login/logout'));
			}	
			$this->url="backend/Kategoriberita";		
		}
		function konversimatauang($var){
			$var=str_replace('Rp ', '', $var);
			$var=str_replace('.', '', $var);
			return $var;
		}
		function atributmenu(){
			$data=array(
				'menu'=>'kategoriberita',
				'headline'=>'Daftar Kategori Berita',
				'edit'=>'Edit Kategori Barita',
				'add'=>"Add Kategori Berita",
				'icon'=>'fa fa-newspaper-o',
				'breadcrumb'=>'backend/kategoriberita',
				'action'=>'backend/kategoriberita',
				);
			//KONVERT ARRAY TO OBJECT
			return $data=(object)$data;	
		}		
		function index(){
			if($this->input->post('submit')){
				$data=array(
					'kategori'=>$this->input->post('kategori'),
					'parent_id'=>0,
					'save_date'=>date('Y-m-d H:i' ),
				);
				$simpan=$this->Crud->simpan('kategori_berita',$data);
				if($simpan){
					$this->session->set_flashdata('success','berhasil');
				}else{
					$error=$this->db->error();
					$this->session->set_flashdata('error',$error['message']);
				}												
			}
			$data=array(
				'menu'=>$this->atributmenu(),
				'data'=>$this->Crud->get_data('kategori_berita')->result(),
			);			
			$this->load->view('backend',$data);
		}
		function update(){
			$id=$this->input->post('id');
			$data=array(
				'kategori'=>$this->input->post('kategori'),
				'update_date'=>date('Y-m-d H:i' ),
			);			
			$update=$this->Crud->update('kategori_berita',$id,$data);
			if($update){
				$this->session->set_flashdata('success','berhasil');
			}else{
				$error=$this->db->error();
				$this->session->set_flashdata('error',$error['message']);
			}
			redirect(site_url($this->url));			
		}	
		function hapus($id){
			$hapus=$this->Crud->hapus('kategori_berita',$id);
			if($hapus==true){
				$this->session->set_flashdata('success','berhasil');
			}else{
				$this->session->set_flashdata('error',$hapus);
			}
			redirect(site_url($this->url));
		}
		function edit(){
			$id=$this->input->post('id');
			$data=array(
				'menu'=>$this->atributmenu(),
				'data'=>$this->Crud->edit('kategori_berita',md5($id))->row(),
			);
			$this->load->view('backend/kategoriberita/edit',$data);
			//print_r($data['data']);
		}
	}
?>