<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class Kategoripekerjaan extends CI_Controller
	{
		
		function __construct(){
			parent::__construct();
			$this->load->model('Crud');
			if(($this->session->userdata('level')!=1) AND ($this->session->userdata('login')==true)){
				redirect(site_url('login/logout'));
			}			
		}
		function atributmenu(){
			$data=array(
				'menu'=>'kategoripekerjaan',
				'headline'=>'Kategori Pekerjaan',
				'icon'=>'fa fa-users',
				'breadcrumb'=>'backend/Kategoripekerjaan',
				);
			//KONVERT ARRAY TO OBJECT
			return $data=(object)$data;	
		}
		function index(){
			$data=array(
				'menu'=>$this->atributmenu(),
				// 'headline'=>'Kategori Pekerjaan',
				'kategoripekerjaan'=>$this->Crud->get_data('kategori_pekerjaan')->result(),
				);
			$this->load->view('backend',$data);
			//print_r($data['menu2']->menu);
		}		
		function kategoripekerjaan_simpan(){
			$id=$this->input->post('id');
			if(empty($this->input->post('parent_id'))){
				$parent_id=0;
			}else{
				$parent_id=$this->input->post('parent_id');
			}
			$data=array(
				'nama'=>$this->input->post('nama'),
				'parent_id'=>$parent_id,
			);
			//JIKA ID ADA BERARTI UPDATE DATA
			if(empty($id)){
				$data['save_date']=date('Y-m-d H:i' );
				$simpan=$this->Crud->simpan('kategori_pekerjaan',$data);
				//echo "simpan";	
			}else{
				$data['update_date']=date('Y-m-d H:i');
				$simpan=$this->Crud->update('kategori_pekerjaan',$id,$data);
				//echo "update";
			}
			if($simpan){
				$this->session->set_flashdata('success','berhasil');
			}else{
				$error=$this->db->error();
				$this->session->set_flashdata('error',$error['message']);
			}
			redirect(site_url("Backend/Kategoripekerjaan"));
		}
		function kategoripekerjaan_hapus($id){
			$hapus=$this->Crud->hapus('kategori_pekerjaan',$id);
			if($hapus==true){
				$this->session->set_flashdata('success','berhasil');
			}else{
				$this->session->set_flashdata('error',$hapus);
			}
			redirect(site_url("Backend/Kategoripekerjaan"));
		}
		function kategoripekerjaan_edit(){
			$id=$this->input->post('id');
			$data=array(
				'menu'=>$this->atributmenu(),
				'kategoripekerjaan'=>$this->Crud->edit('kategori_pekerjaan',md5($id))->row(),
				'parent'=>$this->Crud->get_data('kategori_pekerjaan')->result(),
			);
			$this->load->view('Backend/kategoripekerjaan/edit',$data);
		}
		function get_data(){
			$data=array(
				'kategoripekerjaan'=>$this->Crud->get_data('kategori_pekerjaan')->result(),
				);
			// $data={
			// 	"id":1,
			// 	"text":"My Documents",
			// 	"children":[{
			// 		"id":11,
			// 		"text":"Photos",
			// 		"state":"closed",
			// 		"children":[{
			// 			"id":111,
			// 			"text":"Friend"
			// 		},{
			// 			"id":112,
			// 			"text":"Wife"
			// 		},{
			// 			"id":113,
			// 			"text":"Company"
			// 		}]
			// 	},{
			// 		"id":12,
			// 		"text":"Program Files",
			// 		"children":[{
			// 			"id":121,
			// 			"text":"Intel"
			// 		},{
			// 			"id":122,
			// 			"text":"Java",
			// 			"attributes":{
			// 				"p1":"Custom Attribute1",
			// 				"p2":"Custom Attribute2"
			// 			}
			// 		},{
			// 			"id":123,
			// 			"text":"Microsoft Office"
			// 		},{
			// 			"id":124,
			// 			"text":"Games",
			// 			"checked":true
			// 		}]
			// 	},{
			// 		"id":13,
			// 		"text":"index.html"
			// 	},{
			// 		"id":14,
			// 		"text":"about.html"
			// 	},{
			// 		"id":15,
			// 		"text":"welcome.html"
			// 	}]
			// }];
			echo json_encode($data['kategoripekerjaan']);
		}
	}
?>