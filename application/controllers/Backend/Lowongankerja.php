<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class Lowongankerja extends CI_Controller
	{
		
		function __construct(){
			parent::__construct();
			$this->load->model('Crud');
			if(($this->session->userdata('level')!=1) AND ($this->session->userdata('login')==true)){
				redirect(site_url('login/logout'));
			}		
		}
		function konversimatauang($var){
			$var=str_replace('Rp ', '', $var);
			$var=str_replace('.', '', $var);
			return $var;
		}
		function atributmenu(){
			$data=array(
				'menu'=>'lowongankerja',
				'headline'=>'Lowongan Kerja',
				'edit'=>'Edit Lowongan Kerja',
				'add'=>"Add Lowongan Kerja",
				'icon'=>'fa fa-bars',
				'breadcrumb'=>'backend/Lowongankerja',
				);
			//KONVERT ARRAY TO OBJECT
			return $data=(object)$data;	
		}		
		function index(){
			$lowongankerja=$this->Crud->get_data('lowongankerja')->result();
			foreach ($lowongankerja as $key => $row) {
				$data[$key]=$row;
				$kategori=explode(',', $row->id_kategori);
					$i=0;
					foreach ($kategori as $val) {
						$select='id,nama';
						$tabel='kategori_pekerjaan';
						$where=array('id'=>$val);
						$query=$this->Crud->get_where($select,$tabel,$where)->row();
						$kategoripekerjaan[]=$query->nama;
					}
				$data[$key]->kategori=implode(', ', $kategoripekerjaan);	
			}
			$data=array(
				'menu'=>$this->atributmenu(),
				'lowongankerja'=>$data,
				'kategoripekerjaan'=>$this->Crud->get_data('kategori_pekerjaan')->result(),
				);
			$this->load->view('backend',$data);
			//print_r($data);
		}	
		function lowongankerja_update(){
			$id=$this->input->post('id');
			$id_kategori=implode(',',$this->input->post('id_kategori'));

			$data=array(
				'nama'=>$this->input->post('nama'),
				'id_kategori'=>$id_kategori,
				'perusahaan'=>$this->input->post('perusahaan'),
				'min_gaji'=>$this->konversimatauang($this->input->post('min_gaji')),
				'max_gaji'=>$this->konversimatauang($this->input->post('max_gaji')),
				'tgl_penutupan'=>date('Y-m-d',strtotime($this->input->post('tgl_penutupan'))),
				'deskripsi'=>$this->input->post('editor'),
				'update_date'=>date('Y-m-d H:i'),
			);	
			$simpan=$this->Crud->update('lowongankerja',$id,$data);
			if($simpan){
				$this->session->set_flashdata('success','berhasil');
			}else{
				$error=$this->db->error();
				$this->session->set_flashdata('error',$error['message']);
			}
			redirect(site_url("backend/lowongankerja"));					
		}
		function lowongankerja_hapus($id){
			$hapus=$this->Crud->hapus('lowongankerja',$id);
			if($hapus==true){
				$this->session->set_flashdata('success','berhasil');
			}else{
				$this->session->set_flashdata('error',$hapus);
			}
			redirect(site_url("backend/Lowongankerja"));
		}
		function lowongankerja_edit(){
			$id=$this->input->post('id');
			$data=array(
				'menu'=>$this->atributmenu(),
				'kategoripekerjaan'=>$this->Crud->get_data('kategori_pekerjaan')->result(),
				'data'=>$this->Crud->edit('lowongankerja',md5($id))->row(),
			);
			//print_r($data['data']);
			$this->load->view('backend/lowongankerja/edit',$data);
		}
		function get_data(){
			$data=array(
				'lowongankerja'=>$this->Crud->get_data('lowongankerja')->result(),
				);
			echo json_encode($data['lowongankerja']);
		}
	}
?>