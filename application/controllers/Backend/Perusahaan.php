<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class Perusahaan extends CI_Controller
	{
		
		function __construct(){
			parent::__construct();
			$this->load->model('Crud');
			if(($this->session->userdata('level')!=1) AND ($this->session->userdata('login')==true)){
				redirect(site_url('login/logout'));
			}		
		}
		function konversimatauang($var){
			$var=str_replace('Rp ', '', $var);
			$var=str_replace('.', '', $var);
			return $var;
		}
		function atributmenu(){
			$data=array(
				'menu'=>'perusahaan',
				'headline'=>'Daftar Perusahaan',
				'edit'=>'Edit Perusahaan',
				'add'=>"Add Perusahaan",
				'icon'=>'fa fa-building',
				'breadcrumb'=>'backend/perusahaan',
				);
			//KONVERT ARRAY TO OBJECT
			return $data=(object)$data;	
		}		
		function index(){
			if($this->input->post('submit')){
				$data=array(
					'nama'=>$this->input->post('perusahaan'),
					'email'=>$this->input->post('email'),
					'no_tlp'=>$this->input->post('no_hp'),
					'alamat'=>$this->input->post('alamat'),
					'keterangan'=>$this->input->post('editor'),
					'save_date'=>date('Y-m-d H:i' ),
				);
				$simpan=$this->Crud->simpan('perusahaan',$data);
				if($simpan){
					$this->session->set_flashdata('success','berhasil');
				}else{
					$error=$this->db->error();
					$this->session->set_flashdata('error',$error['message']);
				}
				// redirect(site_url("Perusahaan"));												
			}
			$data=array(
				'menu'=>$this->atributmenu(),
				'lowongankerja'=>$this->Crud->get_data('perusahaan')->result(),
			);			
			$this->load->view('backend',$data);
		}
		function perusahaan_update(){
			$id=$this->input->post('id');
			$data=array(
				'nama'=>$this->input->post('perusahaan'),
				'email'=>$this->input->post('email'),
				'no_tlp'=>$this->input->post('no_hp'),
				'alamat'=>$this->input->post('alamat'),
				'keterangan'=>$this->input->post('editor'),
				'update_date'=>date('Y-m-d H:i' ),
			);			
			$update=$this->Crud->update('perusahaan',$id,$data);
			if($update){
				$this->session->set_flashdata('success','berhasil');
			}else{
				$error=$this->db->error();
				$this->session->set_flashdata('error',$error['message']);
			}
			redirect(site_url("backend/Perusahaan"));			
		}	
		function perusahaan_simpan(){
			if($this->input->post('submit')){

			}else{
				$id=$this->input->post('id');
				$id_kategori=implode(',',$this->input->post('id_kategori'));

				$data=array(
					'nama'=>$this->input->post('nama'),
					'id_kategori'=>$id_kategori,
					'perusahaan'=>$this->input->post('perusahaan'),
					'min_gaji'=>$this->konversimatauang($this->input->post('min_gaji')),
					'max_gaji'=>$this->konversimatauang($this->input->post('max_gaji')),
					'tgl_penutupan'=>date('Y-m-d',strtotime($this->input->post('tgl_penutupan'))),
					'deskripsi'=>$this->input->post('editor'),
				);
				if(empty($id)){
					$data['save_date']=date('Y-m-d H:i' );
					$simpan=$this->Crud->simpan('lowongankerja',$data);
					//echo "simpan";	
					
				}else{
					$data['update_date']=date('Y-m-d H:i');
					$simpan=$this->Crud->update('lowongankerja',$id,$data);
					//echo "upate";
				}
				if($simpan){
					$this->session->set_flashdata('success','berhasil');
				}else{
					$error=$this->db->error();
					$this->session->set_flashdata('error',$error['message']);
				}
				redirect(site_url("lowongankerja"));
				//print_r($data);
			}
		}
		function perusahaan_hapus($id){
			$hapus=$this->Crud->hapus('perusahaan',$id);
			if($hapus==true){
				$this->session->set_flashdata('success','berhasil');
			}else{
				$this->session->set_flashdata('error',$hapus);
			}
			redirect(site_url("backend/perusahaan"));
		}
		function perusahaan_edit(){
			$id=$this->input->post('id');
			$data=array(
				'menu'=>$this->atributmenu(),
				'data'=>$this->Crud->edit('perusahaan',md5($id))->row(),
			);
			$this->load->view('backend/perusahaan/edit',$data);
			//print_r($data['data']);
		}
		// function get_data(){
		// 	$data=array(
		// 		'lowongankerja'=>$this->Crud->get_data('lowongankerja')->result(),
		// 		);
		// 	echo json_encode($data['lowongankerja']);
		// }
	}
?>