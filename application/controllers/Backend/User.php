<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class User extends CI_Controller
	{
		
		function __construct(){
			parent::__construct();
			$this->load->model('Crud');
			if($this->session->userdata('level')!=1){
				redirect(site_url('Login/logout'));
			}
		}
		function atributmenu(){
			$data=array(
				'menu'=>'user',
				'headline'=>'Tampil User',
				'icon'=>'fa fa-users',
				'breadcrumb'=>'backend/User',
				);
			//KONVERT ARRAY TO OBJECT
			return $data=(object)$data;	
		}		
		function index(){
			$data=array(
				'menu'=>$this->atributmenu(),
				'user'=>$this->Crud->get_data('user')->result(),
				);
			$this->load->view('backend',$data);
		}		
		function user_simpan(){
			$id=$this->input->post('id');
			$data=array(
				'nama'=>$this->input->post('nama'),
				'username'=>$this->input->post('username'),
				'password'=>md5($this->input->post('password')),
				'level'=>$this->input->post('level'),
			);
			if(empty($id)){
				$data['save_date']=date('Y-m-d H:i' );
				$simpan=$this->Crud->simpan('user',$data);
				//echo "simpan";	
			}else{
				$data['update_date']=date('Y-m-d H:i');
				$simpan=$this->Crud->update('user',$id,$data);
				//echo "upate";
			}
			if($simpan){
				$this->session->set_flashdata('success','berhasil');
			}else{
				$error=$this->db->error();
				$this->session->set_flashdata('error',$error['message']);
			}
			redirect(site_url("Dashboard/user"));
		}
		function user_hapus($id){
			$hapus=$this->Crud->hapus('user',$id);
			if($hapus==true){
				$this->session->set_flashdata('success','berhasil');
			}else{
				$this->session->set_flashdata('error',$hapus);
			}
			redirect(site_url("Dashboard/user"));
		}
		function user_edit(){
			$id=$this->input->post('id');
			$data=array(
				'menu'=>'User',
				'user'=>$this->Crud->edit('user',$id)->row(),
			);
			$this->load->view('user/edit',$data);
		}
		function get_data(){
			$data=array(
				'user'=>$this->Crud->get_data('user')->result(),
				);
			echo json_encode($data['user']);
		}
	}
?>