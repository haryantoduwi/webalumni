<?php

	class Login extends CI_Controller
	{
		
		function __construct(){
			parent::__construct();
			$this->load->model('mlogin');
		}
		function index(){
			if($this->session->userdata('login')==true){
				redirect(site_url('Dashboard'));
			}
			$this->load->view('login');
		}
		function login_proses(){
            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required',
                   array('required' => 'You must provide a %s.')
            );
			if ($this->form_validation->run() == TRUE){             			
			$username=$this->input->post('username');
			$password=$this->input->post('password');			
			//$data=$this->mlogin->cek_kolom("nama",$where)->row();
			//$cek=$this->mlogin->cek_login("admin",$where)->num_rows();
			//$cek=$this->mlogin->cek($username,$password);
			//if($cek->num_rows() == 1 ){

				if($username=="admin" AND $password=="admin"){
					$sess_data=array(
							'username'=>"admin",
							'level'=>"1",
							'login'=>true,
							);
						$this->session->set_userdata($sess_data);				
					// if($this->session->userdata('level')=='operator'){
					//   redirect(base_url("Coperator"));	
					// }else{
					//   redirect(base_url("Dashboard"));
					// }
					redirect(base_url("/Backend/Dashboard"));
				}else{
					$this->session->set_flashdata('error','Username tidak ditemukan');
					$this->load->view('login');
				}
			}else{
				$this->load->view('login');
			}	
		}
		function logout(){
			$this->session->sess_destroy();
			redirect(site_url('Login'));
		}	
	
	}
?>