<?php
	class Crud extends CI_Model{
		function simpan($tabel,$data){
			$query=$this->db->insert($tabel,$data);
			if($query){
				return true;
			}else{
				return false;
			}
		}
		function get_data($tabel){
			$query=$this->db->get($tabel);
			return $query;
		}
		function get_where($select,$tabel,$where){
			$this->db->select($select);
			$this->db->where($where);
			$query=$this->db->get($tabel);
			return $query;
		}		
		function hapus($tabel,$id){
			$this->db->where('id',$id);
			$query=$this->db->delete($tabel);
			if($query){
				return true;
			}else{
				$this->db->error();
				return $this->db->error('message');
			}
		}
		function edit($tabel,$id){
			$query=$this->db->get_where($tabel,array('md5(id::text)'=>$id));
			return $query;
		}
		function update($tabel,$id,$data){
			$this->db->where('id',$id);
			$query=$this->db->update($tabel,$data);
			if($query){
				return true;
			}else{
				return false;
			}
		}		
	}
?>