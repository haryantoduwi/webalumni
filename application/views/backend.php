<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Dashboard | Admin</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- ComboTree -->

  <link rel="stylesheet" href="<?php  echo base_url();?>asset/plugins/combotree/icon.css">  
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php  echo base_url();?>asset/bootstrap/css/bootstrap.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php  echo base_url();?>asset/plugins/select2/select2.min.css">    
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php  echo base_url();?>asset/plugins/datepicker/datepicker3.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php  echo base_url();?>asset/fontawesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php  echo base_url();?>asset/ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php  echo base_url();?>asset/plugins/datatables/dataTables.bootstrap.css">
        
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php  echo base_url();?>asset/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?php  echo base_url();?>asset/dist/css/sweetalert.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php  echo base_url();?>asset/dist/css/skins/_all-skins.min.css">
</head>
<body class="hold-transition skin-green sidebar-mini fixed">
<!-- Site wrapper -->
<div class="wrapper">
<!--================================HEADER================================-->
  <header class="main-header">
    <!-- Logo -->
    <a href="<?= site_url('Backend/dashboard') ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>D</b>SB</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Dashboard</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li>
            <a href="#" style="text-decoration:none;pointer-events:none">
              <i class="fa fa-bell-o"></i>
               <?= date('H:i - d-m-Y')?>
            </a>
          </li>
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php  echo base_url();?>asset/dist/img/duwi.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs">Haryanto Duwi</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php  echo base_url();?>asset/dist/img/duwi.jpg" class="img-circle" alt="User Image">

                <p>
                  Admininistrator
                  <small><?php echo "Tanggal : " .date('d-m-Y')?></small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="">
                  <a href="<?php echo site_url("Login/logout")?>" class="btn btn-block btn-default btn-flat">Log Out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li><a href="<?php echo site_url('frontend')?>"><i class="fa fa-eye"></i> <span> Halaman Depan</span></a></li>
        <li class="<?php if($menu->menu=='dashboard'){echo 'active';}?>"><a href="<?= site_url('Backend/Dashboard')?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
        <li class="<?php if($menu->menu=='pendaftar'){echo 'active';}?>"><a href="<?= site_url('Backend/Alumni')?>"><i class="fa fa-user"></i> <span>Alumni</span></a></li>
        <li class="treeview <?php if($menu->menu=='lowongankerja'|$menu->menu=='kategoripekerjaan'){echo 'active';}?>">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Lowongan Kerja</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php if($menu->menu=='kategoripekerjaan'){echo 'active';}?>"><a href="<?= site_url('Backend/Kategoripekerjaan')?>"><i class="fa fa-circle-o"></i> Kategori</a></li>
            <li class="<?php if($menu->menu=='lowongankerja'){echo 'active';}?>"><a href="<?= site_url('Backend/Lowongankerja')?>"><i class="fa fa-tasks"></i> <span>Lowongan Kerja</span></a></li>
          </ul>
        </li>
        <li class="<?php if($menu->menu=='perusahaan'){echo 'active';}?>"><a href="<?= site_url('Backend/perusahaan')?>"><i class="fa fa-building"></i> <span>Perusahaan</span></a></li>        
        <li class="<?php if($menu->menu=='agenda'){echo 'active';}?>"><a href="<?= site_url('Backend/agenda')?>"><i class="fa fa-calendar-check-o"></i> <span>Agenda</span></a></li>
        <li class="treeview <?php if($menu->menu=='berita'|$menu->menu=='kategoriberita'){echo 'active';}?>">
            <a href="#">
                <i class="fa fa-newspaper-o"></i> <span>Berita</span>
                <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li class="<?php if($menu->menu=='kategoriberita'){echo 'active';}?>"><a href="<?= site_url('Backend/Kategoriberita')?>"><i class="fa fa-circle-o"></i> Kategori</a></li>
                <li class="<?php if($menu->menu=='berita'){echo 'active';}?>"><a href="<?= site_url('Backend/Berita')?>"><i class="fa  fa-newspaper-o"></i> <span>Berita</span></a></li>
            </ul>
        </li>        
        <li class="<?php if($menu=='user'){echo 'active';}?>"><a href="<?= site_url('Backend/User')?>"><i class="fa fa-users"></i> <span>User</span></a></li>
        <li><a href="#"><i class="fa fa-download"></i> <span>Panduan</span></a></li>      
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <span class="<?= $menu->icon?>"></span>
        <?php echo ucwords($menu->headline)?>
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= site_url('backend/Dashboard')?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="<?= site_url($menu->breadcrumb)?>"><?php echo ucwords($menu->headline)?></a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!--==================================== KODE TULIS DISINI========================================-->  
      <?php
        if($this->session->flashdata('success')){
          echo'
            <div class="alert alert-success alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-ban"></i> Perhatian!</h4>'.
                $this->session->flashdata('success')
            .'</div>
          ';
        }elseif($this->session->flashdata('error')){
          echo'
            <div class="alert alert-error alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-ban"></i> Perhatian!</h4>'.
                $this->session->flashdata('error')
            .'</div>
          ';
        }
        switch ($menu->menu) {
            case 'dashboard':
                include(APPPATH."views/backend/dashboard/form.php");
                break;
            case 'kategoripekerjaan':
                include(APPPATH."views/backend/kategoripekerjaan/index.php");
                break;                
            case 'lowongankerja':
                include(APPPATH."views/backend/lowongankerja/index.php");
                break;  
            case 'perusahaan':
                include(APPPATH."views/backend/perusahaan/index.php");
                break;                                
            case 'user':
                include(APPPATH."views/backend/user/index.php");
                break;
            case 'pendaftar':
                include(APPPATH."views/backend/pendaftar/index.php");
                break; 
            case 'agenda':
                include(APPPATH."views/backend/agenda/index.php");
                break;  
            case 'kategoriberita':
                include(APPPATH."views/backend/kategoriberita/index.php");
                break;
            case 'berita':
                include(APPPATH."views/backend/berita/index.php");
                break;                                                                      
            default:
                # code...
                break;
        }
      ?>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.5
    </div>
    <strong>Copyright &copy; 2017 - <?= date('Y')?> <a href="#">YourName</a>.</strong>
  </footer>
</div>
<!-- ./wrapper -->
<!--=============================FOOTER====================================-->
<!-- jQuery 2.2.3 -->
<!--
<script src="<?php  echo base_url();?>asset/plugins/jQuery/jquery-2.2.3.min.js"></script>
-->
<script src="<?php  echo base_url();?>asset/plugins/jQuery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php  echo base_url();?>asset/bootstrap/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="<?php  echo base_url();?>asset/plugins/select2/select2.full.min.js"></script>
<!-- bootstrap datepicker -->
<script src="<?php  echo base_url();?>asset/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- SlimScroll -->
<script src="<?php  echo base_url();?>asset/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- DataTables -->
<script src="<?php  echo base_url();?>asset/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php  echo base_url();?>asset/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- CK Editor -->
<script src="<?php  echo base_url();?>asset/plugins/ckeditor/ckeditor.js"></script>
<!-- ComboTree-->
<script src="<?php  echo base_url();?>asset/plugins/combotree/jquery.easyui.min.js"></script>
<!-- FastClick -->
<script src="<?php  echo base_url();?>asset/plugins/fastclick/fastclick.js"></script>
<!-- Price Format-->
<script src="<?php  echo base_url();?>asset/dist/js/jquery.priceformat.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php  echo base_url();?>asset/dist/js/app.min.js"></script>
<script src="<?php  echo base_url();?>asset/dist/js/sweetalert.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php  echo base_url();?>asset/dist/js/demo.js"></script>
</body>
</html>
<script type="text/javascript">
  $(document).ready(function(){
    $(".price").priceFormat({
      prefix:'Rp ',
      thousandsSeparator:'.',
      centsLimit:'0',
    });
    $(".btntambah").click(function(){
      $("#tambahdata").toggle();
      $("#tampildata").toggle();
    });
    $(".select2").select2();
    //Date picker
    $('.datepicker').datepicker({
      autoclose: true,
      format:'dd-mm-yyyy',
      todayHighlight: true,
      todayBtn: "linked",
    });    
    $('.datatabel').DataTable(); 
    //EDIT MODAL   
    $('.edit').click(function(){
      var link=$(this).attr('link');
      var id=$(this).attr('id');
      $.ajax({
        type:'POST',
        url:link,
        data:{id:id},
        success:function(data){
          $('#edit').html(data);
          $('#edit').modal('show',{backdrop:'true'});
          $(".select2").select2();
        }
      })
    })
    $('.editdata').click(function(){
      var link=$(this).attr('link');
      var id=$(this).attr('id');
      $.ajax({
        type:'POST',
        url:link,
        data:{id:id},
        success:function(data){
            $('#editdata').html(data);
            $("#tampildata").hide();      
        }
      })
      return false;
    })   
    $('.hapus').click(function(){
      var link=$(this).attr('href');
      swal({
        title:'Perhatian',
        text:'Hapus Data',
        html:true,
        ConfirmButtonColor:'#d9534F',
        showCancelButton:true,
        type:'warning'
      },function(){
        window.location.href=link
      });
      return false
    })  
    //CKEDITOR
    CKEDITOR.replace('editor')  
  })
</script>
