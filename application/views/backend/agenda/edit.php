<div class="row">
    <div class="col-sm-12">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title"><?= ucwords($menu->edit)?></h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
            <form action="<?= site_url($menu->action.'/update')?>" method="POST">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label>Id</label>
                            <input type="text" class="form-control" readonly name="id" value="<?= $data->id?>">
                        </div>
                        <div class="form-group">
                            <label>Judul</label>
                            <input type="text" class="form-control" name="nama" value="<?= $data->nama?>">
                        </div>
                        <div class="form-group">
                            <label>Tanggal</label>
                            <input required type="text" class="datepicker form-control" name="tanggal" value="<?= date('d-m-Y',strtotime($data->tanggal))?>">
                        </div>                                                      
                    </div>                       
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                        <label>Deskripsi</label>
                            <textarea  name="editor" rows="10" cols="80" placeholder="Deskripsi lowongan kerja">    
                                <?= $data->keterangan?>
                            </textarea>                                
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <a href="<?= site_url($menu->action)?>" type="button" class="btntambah btn btn-flat btn-block btn-danger">Back</a>
                <button type="submit" value="submit" name="submit" class="btn btn-flat btn-block btn-primary">Update</button>
            </div>
            </form>
        </div>
    </div>            
</div>
<script type="text/javascript">
    $('.datepicker').datepicker({
        autoclose: true,
        format:'dd/mm/yyyy',
        todayHighlight: true,
        todayBtn: "linked",
    })
    $('.select2').select2();
    CKEDITOR.replace('editor')             
</script>
