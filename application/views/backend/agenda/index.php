<div id="editdata" >
<!--EDIT DATA-->
</div>
<div id="tambahdata" style="display:none">
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= ucwords($menu->add)?></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
                <form action="<?= site_url($menu->action)?>" method="POST">
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-group">
                                <label>Id</label>
                                <input type="text" class="form-control" readonly value="Autogenerate">
                            </div>
                            <div class="form-group">
                                <label>Judul</label>
                                <input type="text" class="form-control" name="nama">
                            </div>
                            <div class="form-group">
                                <label>Tanggal</label>
                                <input required type="text" class="datepicker form-control" name="tanggal">
                            </div>                                                      
                        </div>                       
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                            <label>Deskripsi</label>
                               <textarea  name="editor" rows="10" cols="80" placeholder="Deskripsi lowongan kerja">    
                                </textarea>                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="button" class="btntambah btn btn-flat btn-block btn-danger">Back</button>
                    <button type="submit" value="submit" name="submit" class="btn btn-flat btn-block btn-primary">Simpan</button>
                </div>
                </form>
            </div>
        </div>            
    </div>
</div>
<div id="tampildata">
    <div class="row">
        <div class="col-sm-2">
            <div class="form-group">
                <button data-toggle="modal" data-target="#modaltambah"  class="btntambah btn btn-flat btn-primary btn-block"><span class="fa fa-edit"></span> Tambah</button>
            </div>                     
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title"><?= ucwords($menu->headline)?></h3>
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <table width="100%" class="datatabel table table-hover table-border">
                            <thead style="background-color:black;color:white">
                                <td width="5%">No</td>
                                <td width="15%">Tanggal</td>
                                <td width="30%">Judul</td>
                                <td width="40%">Deskripsi</td>
                                <td class="text-center" width="10%">Action</td>
                            </thead>
                            <tbody>
                                <?php $i=1;foreach ($data as $val): ?>
                                    <tr>
                                        <td><?= $i?></td>
                                        <td><?= date('d-m-Y',strtotime($val->tanggal))?></td>
                                        <td><?= ucwords($val->nama)?></td>
                                        <td><?= substr($val->keterangan, 0,50)?></td>
                                        <td class="text-center">
                                            <a href="#" class="editdata btn btn-flat btn-xs btn-warning" link="<?= site_url($menu->action.'/edit')?>" id="<?=$val->id?>"><span class="fa fa-pencil"></span></a>
                                            <a href="<?= site_url($menu->action.'/hapus/'.$val->id)?>" class="hapus btn btn-xs btn-flat btn-danger"><span class="fa fa-trash"></span></a>
                                        </td>
                                    </tr>
                                <?php $i++; endforeach; ?>                      
                            </tbody>
                        </table>
                    </div>            
                </div>
            </div>
        </div>
    </div>    
</div>