<div class="row">
    <div class="col-sm-12">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title"><?= ucwords($menu->edit)?></h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
            <form action="<?= site_url($menu->action.'/update')?>" method="POST">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label>Id</label>
                            <input type="text" class="form-control" name="id" readonly value="<?= $data->id?>">
                        </div>
                        <div class="form-group">
                            <label>Judul</label>
                            <input required type="text" class="form-control" name="judul" value="<?= ucwords($data->judul)?>">
                        </div>
                        <div class="form-group">
                            <label>Kategori Berita</label>
                            <select class="form-control select2" name="id_kategori[]" multiple="multiple" data-placeholder="Pilih Kategori" style="width:100%"> 
                                <?php
                                    $id_kategori=explode(',', $data->id_kategori);
                                    for($i=0;$i<count($kategori);$i++){
                                        if(isset($id_kategori[$i])){
                                            echo "<option value='".$kategori[$i]->id."' selected>".$kategori[$i]->kategori."</option>";
                                        }else{
                                            echo "<option value='".$kategori[$i]->id."'>".$kategori[$i]->kategori."</option>";
                                        }
                                    }
                                ?>
                            </select>   
                            <?php print_r($id_kategori)?> 
                        </div>                            

                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Deskripsi</label>
                                <textarea  name="editor" rows="10" cols="80" placeholder="Deskripsi lowongan kerja"> 
                                    <?=$data->deskripsi?>   
                                </textarea>
                        </div>                        
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <a href="<?= site_url($menu->breadcrumb)?>" type="button" class="btntambah btn btn-flat btn-block btn-danger">Kembali</a>
                <button type="submit" class="btn btn-flat btn-block btn-primary">Update</button>
            </div>
            </form>
        </div>
    </div>            
</div>
<script type="text/javascript">
    $('.datepicker').datepicker({
        autoclose: true,
        format:'dd/mm/yyyy',
        todayHighlight: true,
        todayBtn: "linked",
    })
    $('.select2').select2();
    CKEDITOR.replace('editor')             
</script>
