<div class="modal-dialog ">
   <div class="modal-content">
       <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?= ucwords($menu->headline)?></h4>
        </div>
        <form method="POST" action="<?= site_url($menu->action.'/update')?>">
        <div class="modal-body">
            <div class="form-group">
                <label>Id</label>
                <input readonly placeholder="Autogenerate" type="text" name="id" value="<?= $data->id?>" class="form-control">
            </div>
            <div class="form-group">
                <label>Kategori</label>
                <input required type="text" name="kategori" class="form-control" value="<?= $data->kategori?>">
            </div>                                                             
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger btn-block btn-flat" data-dismiss="modal">Tutup</button>
            <button type="submit" name="submit" value="submit" class="btn btn-primary btn-block btn-flat">Update</button>
        </div>
        </form>
    </div>
</div>