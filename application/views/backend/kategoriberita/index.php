<div id="tampildata">
    <div class="row">
        <div class="col-sm-2">
            <div class="form-group">
                <button data-toggle="modal" data-target="#modaltambah"  class="btn btn-flat btn-primary btn-block"><span class="fa fa-edit"></span> Tambah</button>
            </div>                     
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title"><?= ucwords($menu->headline)?></h3>
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <table width="100%" class="datatabel table table-hover table-border">
                            <thead style="background-color:black;color:white">
                                <td width="5%">No</td>
                                <td width="55%">Kategori</td>
                                <td width="30%">Parent Id</td>
                                <td class="text-center" width="10%">Action</td>
                            </thead>
                            <tbody>
                                <?php $i=1;foreach ($data as $val): ?>
                                    <tr>
                                        <td><?= $i?></td>
                                        <td><?= ucwords($val->kategori)?></td>
                                        <td><?= $val->parent_id?></td>
                                        <td class="text-center">
                                            <a href="#" class="edit btn btn-flat btn-xs btn-warning" link="<?= site_url($menu->action.'/edit')?>" id="<?=$val->id?>"><span class="fa fa-pencil"></span></a>
                                            <a href="<?= site_url($menu->action.'/hapus/'.$val->id)?>" class="hapus btn btn-xs btn-flat btn-danger"><span class="fa fa-trash"></span></a>
                                        </td>
                                    </tr>
                                <?php $i++; endforeach; ?>                      
                            </tbody>
                        </table>
                    </div>            
                </div>
            </div>
        </div>
    </div>    
</div>
<!--/////////////////////////////////////////////-->
<!--///////////// POP UP MENU ///////////////////-->
<!--/////////////////////////////////////////////-->
<div id="modaltambah" class="modal fade">
    <div class="modal-dialog ">
       <div class="modal-content">
           <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?= ucwords($menu->headline)?></h4>
            </div>
            <form method="POST" action="<?= site_url($menu->action)?>">
            <div class="modal-body">
                <div class="form-group">
                    <label>Id</label>
                    <input disabled placeholder="Autogenerate" type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label>Kategori</label>
                    <input required type="text" name="kategori" class="form-control">
                </div>                                                             
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-block btn-flat" data-dismiss="modal">Tutup</button>
                <button type="submit" name="submit" value="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div id="edit" class="modal fade">
</div>