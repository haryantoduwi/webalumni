<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?= ucwords($menu->headline)?></h4>			
		</div>
		<form action="<?= site_url('backend/kategoripekerjaan/kategoripekerjaan_simpan')?>" method="POST">
		<div class="modal-body">
			<div class="form-group">
				<label>Id</label>
				<input type="text" name="id" class="form-control" readonly value="<?= $kategoripekerjaan->id?>">
			</div>
			<div class="fom-group">
				<label>Nama</label>
				<input type="text" class="form-control" name="nama" value="<?= $kategoripekerjaan->nama?>">
			</div>
			<div class="form-group">
				<label>Parent</label>
				<select class="form-control select2" name="parent_id" style="width:100%">
                        <?php foreach($parent as $val):?>
                            <option value="<?= $val->id?>" <?php if($kategoripekerjaan->parent_id==$val->id){echo 'selected';}?>><?= $val->nama?></option>
                        <?php endforeach;?>					
				</select>
			</div>
		</div>
		<div class="modal-footer">
			 <button type="submit" class="btn btn-primary btn-block btn-flat">Update</button>
		</div>
		</form>
	</div>
</div>