<div class="row">
	<div class="col-sm-2">
		<div class="form-group">
			<button id="btntambah" data-toggle="modal" data-target="#modaltambah"  class="btn btn-flat btn-primary btn-block"><span class="fa fa-edit"></span> Tambah</button>
		</div>			
	</div>
</div>
<div class="row" >
	<div class="col-sm-12">
        <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title"><?= ucwords($menu->headline)?></h3>
            </div>
            <div class="box-body">
				<div class="table-responsive">
					<table width="100%" class="datatabel table table-hover">
                        <thead style="background-color:black;color:white">
                            <td width="5%">No</td>
                            <td width="35%">Nama</td>
                            <td width="15%">Parent Id</td>
                            <td width="15%">Tgl.Simpan</td>
                            <td width="15%">Tgl.Update</td>
                            <td class="text-center" width="15%">Action</td>
                        </thead>
						<tbody>
                            <?php $i=1;foreach ($kategoripekerjaan as $val): ?>
                                <tr>
                                    <td><?= $i?></td>
                                    <td><?= $val->nama?></td>
                                    <td><?= $val->parent_id?></td>
                                    <td><?= date('d-m-Y',strtotime($val->save_date))?></td>
                                    <td><?php if(empty($val->update_date)){echo "-";}else{ echo date('d-m-Y',strtotime($val->update_date));}?></td>
                                    <td class="text-center">
                                        <a href="#" id="<?= $val->id?>" link="<?= site_url('Backend/kategoripekerjaan/kategoripekerjaan_edit')?>" class="edit btn btn-xs btn-flat btn-info"><span class="fa fa-pencil"></span></a>
                                        <a href="<?= site_url('backend/Kategoripekerjaan/kategoripekerjaan_hapus/'.$val->id)?>" class="hapus btn btn-xs btn-flat btn-danger"><span class="fa fa-trash"></span></a>
                                    </td>
                                </tr>
                            <?php $i++; endforeach; ?>  					
						</tbody>
					</table>
				</div>            
            </div>
        </div>
	</div>
</div>
<!--/////////////////////////////////////////////-->
<!--///////////// POP UP MENU ///////////////////-->
<!--/////////////////////////////////////////////-->
<div id="modaltambah" class="modal fade">
    <div class="modal-dialog ">
       <div class="modal-content">
           <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?= ucwords($menu->headline)?></h4>
            </div>
            <form method="POST" action="<?= site_url('backend/Kategoripekerjaan/kategoripekerjaan_simpan')?>">
            <div class="modal-body">
                <div class="form-group">
                    <label>Id</label>
                    <input disabled placeholder="Autogenerate" type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label>Nama</label>
                    <input required type="text" name="nama" class="form-control">
                </div>
                <div class="form-group">
                    <label>Parent</label>
                    <select class="form-control select2" name="parent_id" style="width:100%">
                        <?php foreach($kategoripekerjaan as $val):?>
                            <option value="<?= $val->id?>"><?= $val->nama?></option>
                        <?php endforeach;?>
                    </select>
                </div>                                                             
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-block btn-flat" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div id="edit" class="modal fade">

</div>