<div class="row">
    <div class="col-sm-12">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title"><?= ucwords($menu->edit)?></h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
            <form action="<?= site_url('backend/Lowongankerja/lowongankerja_update')?>" method="POST">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label>Id</label>
                            <input type="text" class="form-control" name="id" readonly value="<?= $data->id?>">
                        </div>
                        <div class="form-group">
                            <label>Headline</label>
                            <input required type="text" class="form-control" name="nama" value="<?= $data->nama?>">
                        </div>
                        <div class="form-group">
                            <label>Nama Perusahaan</label>
                            <input required type="text" class="form-control" name="perusahaan" value="<?= $data->perusahaan?>">
                        </div>                            
                        <div class="form-group">
                            <label>Kategori <?php echo($data->id_kategori)?></label>
                            <select class="form-control select2" multiple="multiple" data-placeholder="Pilih kategori" name="id_kategori[]" style="width:100%">
                                <?php
                                    $datax=explode(',', $data->id_kategori);
                                    for($x=0;$x<count($kategoripekerjaan);$x++) {
                                        if(isset($datax[$x])){
                                            echo "<option value='".$kategoripekerjaan[$x]->id."' selected>".$kategoripekerjaan[$x]->nama."</option>";
                                        }else{
                                            echo "<option value='".$kategoripekerjaan[$x]->id."'>".$kategoripekerjaan[$x]->nama."</option>";
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label>Tanggal Terakhir Pendaftaran</label>
                            <input type="text" class="form-control datepicker" name="tgl_penutupan" value="<?= date('d/m/Y',strtotime($data->tgl_penutupan))?>">
                        </div>                        
                        <div class="form-group">
                            <label>Gaji Minimal</label>
                            <input type="text" class="price form-control" name="min_gaji" value="<?= $data->min_gaji?>">
                        </div>
                        <div class="form-group">
                            <label>Gaji Tertinggi</label>
                            <input type="text" class="price form-control" name="max_gaji" value="<?= $data->max_gaji?>">
                        </div>                                                          
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                        <label>Deskripsi</label>
                            <textarea  name="editor" rows="10" cols="80" placeholder="Deskripsi lowongan kerja">    
                                <?= $data->deskripsi?>
                            </textarea>                                
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <a href="<?= site_url($menu->breadcrumb)?>" type="button" class="btntambah btn btn-flat btn-block btn-danger">Kembali</a>
                <button type="submit" class="btn btn-flat btn-block btn-primary">Update</button>
            </div>
            </form>
        </div>
    </div>            
</div>
<script type="text/javascript">
    $('.datepicker').datepicker({
        autoclose: true,
        format:'dd/mm/yyyy',
        todayHighlight: true,
        todayBtn: "linked",
    })
    $('.select2').select2();
    CKEDITOR.replace('editor')             
</script>
