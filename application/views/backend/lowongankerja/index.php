<!--EDIT DATA-->
<div id="editdata">
    
</div>
<!--ADD DATA-->
<div id="tambahdata" style="display:none">
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= ucwords($menu->add)?></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
                <form action="<?= site_url('backend/Lowongankerja/lowongankerja_simpan')?>" method="POST">
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-group">
                                <label>Id</label>
                                <input type="text" class="form-control" readonly value="Autogenerate">
                            </div>
                            <div class="form-group">
                                <label>Headline</label>
                                <input required type="text" class="form-control" name="nama">
                            </div>
                            <div class="form-group">
                                <label>Nama Perusahaan</label>
                                <input required type="text" class="form-control" name="perusahaan">
                            </div>                            
                            <div class="form-group">
                                <label>Kategori</label>
                                <select class="form-control select2" multiple="multiple" data-placeholder="Pilih kategori" name="id_kategori[]" style="width:100%">
                                    <?php foreach($kategoripekerjaan as $val):?>
                                        <option value="<?= $val->id?>"><?= $val->nama?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="form-group">
                                <label>Tanggal Terakhir Pendaftaran</label>
                                <input type="text" class="form-control datepicker" name="tgl_penutupan">
                            </div>                        
                            <div class="form-group">
                                <label>Gaji Minimal</label>
                                <input type="text" class="price form-control" name="min_gaji">
                            </div>
                            <div class="form-group">
                                <label>Gaji Tertinggi</label>
                                <input type="text" class="price form-control" name="max_gaji">
                            </div>                                                          
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                            <label>Deskripsi</label>
                               <textarea  name="editor" rows="10" cols="80" placeholder="Deskripsi lowongan kerja">    
                                </textarea>                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="button" class="btntambah btn btn-flat btn-block btn-danger">Back</button>
                    <button type="submit" class="btn btn-flat btn-block btn-primary">Simpan</button>
                </div>
                </form>
            </div>
        </div>            
    </div>
</div>
<div id="tampildata">
    <div class="row">
        <div class="col-sm-2">
            <div class="form-group">
                <button data-toggle="modal" data-target="#modaltambah"  class="btntambah btn btn-flat btn-primary btn-block"><span class="fa fa-edit"></span> Tambah</button>
            </div>          
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title"><?= ucwords($menu->headline)?></h3>
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <table width="100%" class="datatabel table table-hover table-border">
                            <thead style="background-color:black;color:white">
                                <td width="5%">No</td>
                                <td width="20%">Nama</td>
                                <td width="15%">Kategori</td>
                                <td width="15%">Perusahaan</td>
                                <td width="30%">Keterangan</td>
                                <td class="text-center" width="15%">Action</td>
                            </thead>
                            <tbody>
                                <?php $i=1;foreach ($lowongankerja as $val): ?>
                                    <tr>
                                        <td><?= $i?></td>
                                        <td><?= $val->nama .'<br><small>Terakhir Pendaftaran : </small>' .date('d-m-Y',strtotime($val->tgl_penutupan))?></td>
                                        <td><?= $val->kategori?></td>
                                        <td><?= $val->perusahaan?></td>
                                        <td><?= substr($val->deskripsi, 0,50)?></td>
                                        <td class="text-center">
                                            <a href="#" id="<?= $val->id?>" link="<?= site_url('backend/Lowongankerja/lowongankerja_edit')?>" class="editdata btn btn-xs btn-flat btn-info"><span class="fa fa-pencil"></span></a>
                                            <a href="<?= site_url('backend/Lowongankerja/lowongankerja_hapus/'.$val->id)?>" class="hapus btn btn-xs btn-flat btn-danger"><span class="fa fa-trash"></span></a>
                                        </td>
                                    </tr>
                                <?php $i++; endforeach; ?>                      
                            </tbody>
                        </table>
                    </div>            
                </div>
            </div>
        </div>
    </div>    
</div>
