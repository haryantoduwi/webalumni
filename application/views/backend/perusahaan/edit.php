<div>
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= ucwords($menu->edit)?></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
                <form action="<?= site_url('backend/perusahaan/perusahaan_update')?>" method="POST">
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-group">
                                <label>Id</label>
                                <input type="text" class="form-control" readonly value="<?= $data->id?>" name="id">
                            </div>
                            <div class="form-group">
                                <label>Nama Perushaan</label>
                                <input required type="text" class="form-control datepicker" name="nama" value="<?= $data->nama?>">
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input required type="text" class="form-control" name="email" value="<?= $data->email?>">
                            </div>                            
                            <div class="form-group">
                                <label>No HP</label>
                                <input type="text" name="no_hp" class="form-control" value="<?= $data->no_tlp?>">
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="form-group">
                                <label>Alamat</label>
                                <textarea class="form-control" name="alamat" rows="4"><?= $data->alamat?></textarea>
                            </div> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                            <label>Deskripsi</label>
                               <textarea  name="editor" rows="10" cols="80" placeholder="Deskripsi lowongan kerja">    
                                    <?= $data->keterangan?>
                               </textarea>                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <a href="<?= site_url('backend/perusahaan')?>" class="btn btn-flat btn-block btn-danger">Back</a>
                    <button type="submit" class="btn btn-flat btn-block btn-primary">Simpan</button>
                </div>
                </form>
            </div>
        </div>            
    </div>
</div>
<script type="text/javascript">
    $('.datepicker').datepicker({
        autoclose: true,
        format:'dd/mm/yyyy',
        todayHighlight: true,
        todayBtn: "linked",
    })
    CKEDITOR.replace('editor')             
</script>
