<div id="editdata" >
<!--EDIT DATA-->
</div>
<div id="tambahdata" style="display:none">
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= ucwords($menu->add)?></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
                <form action="<?= site_url('backend/Perusahaan')?>" method="POST">
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-group">
                                <label>Id</label>
                                <input type="text" class="form-control" readonly value="Autogenerate">
                            </div>
                            <div class="form-group">
                                <label>Nama Perushaan</label>
                                <input type="text" class="form-control" name="perusahaan">
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input required type="text" class="form-control" name="email">
                            </div>                            
                            <div class="form-group">
                                <label>No HP</label>
                                <input type="text" name="no_hp" class="form-control">
                            </div>                           
                        </div>
                        <div class="col-sm-7">
                            <div class="form-group">
                                <label>Alamat</label>
                                <textarea class="form-control" name="alamat" rows="4"></textarea>
                            </div> 
                        </div>                        
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                            <label>Deskripsi</label>
                               <textarea  name="editor" rows="10" cols="80" placeholder="Deskripsi lowongan kerja">    
                                </textarea>                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="button" class="btntambah btn btn-flat btn-block btn-danger">Back</button>
                    <button type="submit" value="submit" name="submit" class="btn btn-flat btn-block btn-primary">Simpan</button>
                </div>
                </form>
            </div>
        </div>            
    </div>
</div>
<div id="tampildata">
    <div class="row">
        <div class="col-sm-2">
            <div class="form-group">
                <button data-toggle="modal" data-target="#modaltambah"  class="btntambah btn btn-flat btn-primary btn-block"><span class="fa fa-edit"></span> Tambah</button>
            </div>                     
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title"><?= ucwords($menu->headline)?></h3>
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <table width="100%" class="datatabel table table-hover table-border">
                            <thead style="background-color:black;color:white">
                                <td width="5%">No</td>
                                <td width="20%">Nama</td>
                                <td width="15%">Email</td>
                                <td width="15%">Kontak</td>
                                <td width="15%">Alamat</td>
                                <td width="20%">Keterangan</td>
                                <td class="text-center" width="10%">Action</td>
                            </thead>
                            <tbody>
                                <?php $i=1;foreach ($lowongankerja as $val): ?>
                                    <tr>
                                        <td><?= $i?></td>
                                        <td><?= ucwords($val->nama)?></td>
                                        <td><?= $val->email?></td>
                                        <td><?= $val->no_tlp?></td>
                                        <td><?= $val->alamat?></td>
                                        <td><?= substr($val->keterangan, 0,50)?></td>
                                        <td class="text-center">
                                            <!--
                                            <a href="<?= site_url('backend/perusahaan/perusahaan_edit/'.md5($val->id))?>" id="<?= $val->id?>" link="<?= site_url('backend/Lowongankerja/lowongankerja_edit')?>" class="editdata btn btn-xs btn-flat btn-info"><span class="fa fa-pencil"></span></a>
                                            -->
                                            <a href="#" class="editdata btn btn-flat btn-xs btn-warning" link="<?= site_url('backend/perusahaan/perusahaan_edit')?>" id="<?=$val->id?>"><span class="fa fa-pencil"></span></a>
                                            <a href="<?= site_url('backend/perusahaan/perusahaan_hapus/'.$val->id)?>" class="hapus btn btn-xs btn-flat btn-danger"><span class="fa fa-trash"></span></a>
                                        </td>
                                    </tr>
                                <?php $i++; endforeach; ?>                      
                            </tbody>
                        </table>
                    </div>            
                </div>
            </div>
        </div>
    </div>    
</div>