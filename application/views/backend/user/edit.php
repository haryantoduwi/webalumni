    <div class="modal-dialog ">
       <div class="modal-content">
           <div class="modal-header">
              	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?= ucwords($menu)?></h4>
            </div>
            <form method="POST" action="<?= site_url('User/user_simpan')?>">
            <div class="modal-body">
            	<div class="form-group">
            		<label>Id</label>
            		<input readonly value="<?= $user->id?>" type="text" name="id" class="form-control">
            	</div>
            	<div class="form-group">
            		<label>Nama</label>
            		<input type="text" name="nama" value="<?= $user->nama?>" class="form-control">
            	</div>
            	<div class="form-group">
            		<label>Username</label>
            		<input type="text" name="username" value="<?= $user->username?>" class="form-control">
            	</div>
            	<div class="form-group">
            		<label>Password</label>
            		<input type="password" name="password" value="<?= $user->password?>" class="form-control" >
            	</div>
            	<div class="form-group">
            		<label>Level</label>
            		<select class="form-control select2" name="level" style="width:100%">
            			<option value="1" <?php if($user->level==1){echo 'selected';}?>>Admin</option>
            			<option value="2" <?php if($user->level==2){echo 'selected';}?>>User</option>
            		</select>
            	</div>            	            	            	            	
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Update</button>
            </div>
            </form>
        </div>
    </div>