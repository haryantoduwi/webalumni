<div class="row">
	<div class="col-sm-2">
		<div class="form-group">
			<button id="btntambah" data-toggle="modal" data-target="#modaltambah"  class="btn btn-flat btn-primary btn-block"><span class="fa fa-edit"></span> Tambah</button>
		</div>			
	</div>
</div>
<div class="row" >
	<div class="col-sm-12">
        <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title"><?= ucwords($menu->headline)?></h3>
            </div>
            <div class="box-body">
				<div class="table-responsive">
					<table width="100%" class="datatabel table table-hover ">
                        <thead style="background-color:black;color:white">
                            <td width="5%">No</td>
                            <td width="20%">Nama</td>
                            <td width="15%">Username</td>
                            <td width="15%">Password</td>
                            <td width="15%">Tersimpan</td>
                            <td width="15%">Terupdate</td>
                            <td class="text-center" width="15%">Action</td>
                        </thead>
						<tbody>
                            <?php $i=1;foreach ($user as $val): ?>
                                <tr>
                                    <td><?= $i?></td>
                                    <td><?= $val->nama?></td>
                                    <td><?= $val->username?></td>
                                    <td><?= $val->password?></td>
                                    <td><?= date('d-m-Y H:i',strtotime($val->save_date))?></td>
                                    <td><?php if(empty($val->update_date)){echo '-';}else{echo date('d-m-Y H:i',strtotime($val->update_date));}?></td>
                                    <td class="text-center">
                                        <a href="#" id="<?= $val->id?>" link="<?= site_url('User/user_edit')?>" class="edit btn btn-xs btn-flat btn-info"><span class="fa fa-pencil"></span></a>
                                        <a href="<?= site_url('User/user_hapus/'.$val->id)?>" class="hapus btn btn-xs btn-flat btn-danger"><span class="fa fa-trash"></span></a>
                                    </td>
                                </tr>
                            <?php $i++; endforeach; ?>  					
						</tbody>
					</table>
				</div>            
            </div>
        </div>
	</div>
</div>

<div id="modaltambah" class="modal fade">
    <div class="modal-dialog ">
       <div class="modal-content">
           <div class="modal-header">
              	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?= ucwords($menu)?></h4>
            </div>
            <form method="POST" action="<?= site_url('User/user_simpan')?>">
            <div class="modal-body">
            	<div class="form-group">
            		<label>Id</label>
            		<input disabled placeholder="Autogenerate" type="text" class="form-control">
            	</div>
            	<div class="form-group">
            		<label>Nama</label>
            		<input type="text" name="nama" class="form-control">
            	</div>
            	<div class="form-group">
            		<label>Username</label>
            		<input type="text" name="username" class="form-control">
            	</div>
            	<div class="form-group">
            		<label>Password</label>
            		<input type="password" name="password" class="form-control">
            	</div>
            	<div class="form-group">
            		<label>Level</label>
            		<select class="form-control select2" name="level" style="width:100%">
            			<option value="1">Admin</option>
            			<option value="2">User</option>
            		</select>
            	</div>            	            	            	            	
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-block btn-flat" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div id="edit" class="modal fade">

</div>