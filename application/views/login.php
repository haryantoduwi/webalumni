<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>User Login</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php  echo base_url();?>asset/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php  echo base_url();?>asset/fontawesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php  echo base_url();?>asset/ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php  echo base_url();?>asset/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php  echo base_url();?>asset/plugins/iCheck/square/blue.css">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="<?php echo site_url()?>"><b>User</b>Login</a>
  </div>
  <!-- /.login-logo -->
  <?php
    if(!empty($this->session->flashdata('error'))){
      echo'
        <div class="alert alert-danger alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h4><i class="icon fa fa-ban"></i> Perhatian!</h4>'.
            $this->session->flashdata('error')
        .'</div>
      ';
    }
  ?>
  <div class="login-box-body">
    <p class="login-box-msg">Silahkan login untuk mengakses sistem</p>
    <form action="<?php  echo base_url('Login/login_proses');?>" method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Username" name="username" value="<?= set_value('username')?>">
        <span class="glyphicon glyphicon-user form-control-feedback"></span> 
        <p><?= form_error('username','<div class="text-red">', '</div>'); ?></p>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password" value="<?= set_value('password')?>">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        <p><?= form_error('password','<div class="text-red">', '</div>'); ?></p>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="form-group">
             <button type="submit" class="btn btn-primary btn-block btn-flat">Login</button>
          </div>
        </div>
        <div class="pull-left col-sm-6">
          <div class="form-group">
            <a href="#"><span class="fa fa-download"></span> Panduan</a>
          </div>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>
  <div class="login-logo">
    <p style="font-size:16px;margin-top:20px"><strong>Copyright &copy; 2017 - <?= date('Y')?> <a href="#">YourName</a>.</strong> <br> Versi 1.0</p>
  </div> 
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
<!-- jQuery 2.2.3 -->
<script src="<?php  echo base_url();?>asset/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php  echo base_url();?>asset/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
