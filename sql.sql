/*
Navicat PGSQL Data Transfer

Source Server         : Postgree on Localhost
Source Server Version : 90103
Source Host           : localhost:5432
Source Database       : webalumni
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90103
File Encoding         : 65001

Date: 2018-05-30 13:41:34
*/


-- ----------------------------
-- Sequence structure for alumni_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."alumni_id_seq";
CREATE SEQUENCE "public"."alumni_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for kategori_pekerjaan_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."kategori_pekerjaan_id_seq";
CREATE SEQUENCE "public"."kategori_pekerjaan_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 8
 CACHE 1;
SELECT setval('"public"."kategori_pekerjaan_id_seq"', 8, true);

-- ----------------------------
-- Sequence structure for lowongankerja_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."lowongankerja_id_seq";
CREATE SEQUENCE "public"."lowongankerja_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 3
 CACHE 1;
SELECT setval('"public"."lowongankerja_id_seq"', 3, true);

-- ----------------------------
-- Sequence structure for perusahaan_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."perusahaan_id_seq";
CREATE SEQUENCE "public"."perusahaan_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 2
 CACHE 1;
SELECT setval('"public"."perusahaan_id_seq"', 2, true);

-- ----------------------------
-- Sequence structure for User_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."User_id_seq";
CREATE SEQUENCE "public"."User_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Table structure for alumni
-- ----------------------------
DROP TABLE IF EXISTS "public"."alumni";
CREATE TABLE "public"."alumni" (
"id" int4 DEFAULT nextval('alumni_id_seq'::regclass) NOT NULL,
"nama" varchar(255) COLLATE "default",
"jenis_kelamin" varchar(255) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of alumni
-- ----------------------------

-- ----------------------------
-- Table structure for kategori_pekerjaan
-- ----------------------------
DROP TABLE IF EXISTS "public"."kategori_pekerjaan";
CREATE TABLE "public"."kategori_pekerjaan" (
"id" int4 DEFAULT nextval('kategori_pekerjaan_id_seq'::regclass) NOT NULL,
"nama" varchar(255) COLLATE "default",
"parent_id" int4,
"save_date" date,
"update_date" date
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of kategori_pekerjaan
-- ----------------------------
INSERT INTO "public"."kategori_pekerjaan" VALUES ('3', 'Teknik Infomatika', '0', '2018-05-25', null);
INSERT INTO "public"."kategori_pekerjaan" VALUES ('5', 'Teknik Geologi', '0', '2018-05-25', null);
INSERT INTO "public"."kategori_pekerjaan" VALUES ('6', 'Teknik Industri', '0', '2018-05-25', null);
INSERT INTO "public"."kategori_pekerjaan" VALUES ('7', 'Programer', '3', '2018-05-25', null);
INSERT INTO "public"."kategori_pekerjaan" VALUES ('8', 'System Administratorr', '3', '2018-05-28', '2018-05-28');

-- ----------------------------
-- Table structure for lowongankerja
-- ----------------------------
DROP TABLE IF EXISTS "public"."lowongankerja";
CREATE TABLE "public"."lowongankerja" (
"id" int4 DEFAULT nextval('lowongankerja_id_seq'::regclass) NOT NULL,
"nama" varchar(255) COLLATE "default",
"id_kategori" varchar(16) COLLATE "default",
"perusahaan" varchar(100) COLLATE "default",
"min_gaji" int8,
"max_gaji" int8,
"deskripsi" text COLLATE "default",
"save_date" timestamp(0),
"update_date" timestamp(0),
"tgl_penutupan" date
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of lowongankerja
-- ----------------------------
INSERT INTO "public"."lowongankerja" VALUES ('3', 'Lowongan kerja Programer', '3,7,8', 'PT Indoxsys', '4000000', '8000000', '<p>Lowongan pekerjaan</p>
', '2018-05-28 05:11:00', null, '2018-05-28');

-- ----------------------------
-- Table structure for perusahaan
-- ----------------------------
DROP TABLE IF EXISTS "public"."perusahaan";
CREATE TABLE "public"."perusahaan" (
"id" int4 DEFAULT nextval('perusahaan_id_seq'::regclass) NOT NULL,
"nama" varchar(255) COLLATE "default",
"email" varchar(50) COLLATE "default",
"no_tlp" varchar(20) COLLATE "default",
"alamat" text COLLATE "default",
"keterangan" text COLLATE "default",
"save_date" timestamp(6),
"update_date" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of perusahaan
-- ----------------------------
INSERT INTO "public"."perusahaan" VALUES ('2', 'Pertamina', 'hrd@pertamina.com', '0857876768', 'Jakarta', '<p>wdadadwadwa</p>
', '2018-05-30 08:40:00', null);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS "public"."user";
CREATE TABLE "public"."user" (
"id" int4 DEFAULT nextval('"User_id_seq"'::regclass) NOT NULL,
"username" varchar(255) COLLATE "default",
"password" varchar(255) COLLATE "default",
"save_date" date,
"update_date" date,
"level" varchar(255) COLLATE "default",
"email" varchar(255) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of user
-- ----------------------------

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------
ALTER SEQUENCE "public"."alumni_id_seq" OWNED BY "alumni"."id";
ALTER SEQUENCE "public"."kategori_pekerjaan_id_seq" OWNED BY "kategori_pekerjaan"."id";
ALTER SEQUENCE "public"."lowongankerja_id_seq" OWNED BY "lowongankerja"."id";
ALTER SEQUENCE "public"."perusahaan_id_seq" OWNED BY "perusahaan"."id";
ALTER SEQUENCE "public"."User_id_seq" OWNED BY "user"."id";

-- ----------------------------
-- Primary Key structure for table alumni
-- ----------------------------
ALTER TABLE "public"."alumni" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table kategori_pekerjaan
-- ----------------------------
ALTER TABLE "public"."kategori_pekerjaan" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table lowongankerja
-- ----------------------------
ALTER TABLE "public"."lowongankerja" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table perusahaan
-- ----------------------------
ALTER TABLE "public"."perusahaan" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table user
-- ----------------------------
ALTER TABLE "public"."user" ADD PRIMARY KEY ("id");
